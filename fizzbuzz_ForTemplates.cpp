#include <iostream>
#include <string>

template <int N, bool fizz, bool buzz>
struct _FizzBuzz;

template <int N>
struct FizzBuzz : _FizzBuzz<N, N % 3 == 0, N % 5 == 0>
{
};

template <>
struct FizzBuzz<1>
{
    static std::string str()
    {
        return "1";
    }
};

template <int N>
struct _FizzBuzz<N, true, true>
{
    static std::string str()
    {
        return FizzBuzz<N - 1>::str() + "\nFizzBuzz";
    }

};

template <int N>
struct _FizzBuzz<N, true, false>
{
    static std::string str()
    {
        return FizzBuzz<N - 1>::str() + "\nFizz";
    }

};

template <int N>
struct _FizzBuzz<N, false, true>
{
    static std::string str()
    {
        return FizzBuzz<N - 1>::str() + "\nBuzz";
    }

};

template <int N>
struct _FizzBuzz<N, false, false>
{
    static std::string str()
    {
        return FizzBuzz<N - 1>::str() + "\n" + std::to_string(N);
    }

};


int main(int argc, char** argv)
{
    auto spStr = FizzBuzz<47>::str();
    std::cout << "Your Output: \n" << spStr;
    getchar();
    return 0;
}
